var mongodb = require('mongodb');
var server = new mongodb.Server("127.0.0.1", 27017, {});
var client;
new mongodb.Db('test', server, {}).open(function (error, c) {
  if (error) throw error;
  client = c;
});

var express = require('express');
var app = express();
app.use('/public', express.static('public'));

app.get('/home', function(req, res){
  res.write('<div id="b1"></div><div id="b2"></div><div id="b3"></div><div id="b4"></div><div id="b5"></div>');
  x = ["b1", "b2", "b3", "b4", "b5"]
  res.cnt = x.length
  for (var i = 0; i < x.length; i++) {
    sendScript(res, x[i])
  }
});


function checkRes(res) {
    res.cnt--
    if (res.cnt === 0) {
        res.end();
    }
}
function sendScript(res, id) {
    var collection = new mongodb.Collection(client, 'b1');
    collection.find({}, {limit:10}).toArray(function(err, docs) {
      res.write('<script>');
      res.write('render(');
      res.write('"' + id + '",{data:');
      res.write(JSON.stringify(docs));
      res.write('})');
      res.write('</script>');
      checkRes(res);
    });
}
app.listen(3001);
console.log('Listening on port 3000');